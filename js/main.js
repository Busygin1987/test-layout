function handleClickBurger(element) {
    element.classList.toggle('burger-active');

    var header = document.querySelector('.header-menu-container');
    header.classList.toggle('header-menu-container-active');

    var logo = document.querySelector('.logo');
    logo.classList.toggle('logo-active');

    var phone = document.querySelector('.phone');
    phone.classList.toggle('phone-active');

    var dropDown = document.querySelector('.drop-down');
    dropDown.classList.toggle('drop-down-active');
}

window.addEventListener('scroll', function() {
        var burger = document.querySelector('.burger');
        var header = document.querySelector('.header-menu-container');
        var logo = document.querySelector('.logo');
        var phone = document.querySelector('.phone');

    if (pageYOffset > 20) {
        burger.classList.add('burger-dark');
        header.classList.add('header-menu-container-white');
        logo.classList.add('logo-dark');
        phone.classList.add('phone-dark');
    } else {
        burger.classList.remove('burger-dark');
        header.classList.remove('header-menu-container-white');
        logo.classList.remove('logo-dark');
        phone.classList.remove('phone-dark');
    }
});

window.onload = function () {
    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.slider-control-mobile .swiper-pagination',
            type: 'fraction',
            renderFraction: function (currentClass, totalClass) {
                return '<span class="' + currentClass + '"></span>' + '<span class="pagination-text"> из</span> ' + '<span class="' + totalClass + '"></span>';
            }
        },
        navigation: {
            nextEl: '.slider-control-mobile .swiper-button-next',
            prevEl: '.slider-control-mobile .swiper-button-prev',
        },
        breakpoints: {
            // when window width is >= 320px
            321: {
                pagination: {
                    el: '.slider-control-desktop .swiper-pagination',
                    type: 'fraction',
                    renderFraction: function (currentClass, totalClass) {
                        return '<span class="' + currentClass + '"></span>' + '<span class="pagination-text"> из</span> ' + '<span class="' + totalClass + '"></span>';
                    }
                },
                navigation: {
                    nextEl: '.slider-control-desktop .swiper-button-next',
                    prevEl: '.slider-control-desktop .swiper-button-prev',
                },
            },
          }
    });
};